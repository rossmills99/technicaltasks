﻿(function () {
    var module = angular.module('app', []);
    
    module.controller('MainCtrl', function () {
        this.name = 'John';
        this.dob = {
            day: 1,
            month: 1,
            year: 1999
        };

        this.getAge = function () {
            var now = new Date();
            return now.getFullYear() - this.dob.year;
        };
    });

})();