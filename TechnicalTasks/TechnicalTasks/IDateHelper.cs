﻿using System;

namespace TechnicalTasks
{
    /// <summary>
    /// Helper for Date-related tasks.
    /// </summary>
    internal interface IDateHelper
    {
        /// <summary>
        /// Gets the DateTime closest to the current time (now), formatted as UTC string.
        /// </summary>
        /// <param name="start">Start time.</param>
        /// <param name="end">End time.</param>
        /// <returns></returns>
        string GetNearestUtcDateTimeString(DateTime start, DateTime end);
    }

}
