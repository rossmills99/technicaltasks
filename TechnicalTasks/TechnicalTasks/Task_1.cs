﻿/**
    1. Implement the IDateHelper interface in a new class.
    2. Write the body of DateHelperClientTest using an instance of the new IDateHelper implementation.
*/

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TechnicalTasks
{

    [TestClass]
    public class Task_1
    {
        [TestMethod]
        public void DateHelperClientTest()
        {
            // Write a test of DateHelperClient here. e.g.:
            // IDateHelper dateHelper = new DateHelper();
            // var dateHelperCLient = new DateHelperClient(dateHelper);
            // ... test goes here ...
        }
    }
}
