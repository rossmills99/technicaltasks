﻿using System.Threading.Tasks;

namespace TechnicalTasks
{
    public interface IMessageWriter
    {
        Task WriteToFileAsync(string filename);
    }
}