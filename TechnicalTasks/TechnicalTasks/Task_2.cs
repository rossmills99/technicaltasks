﻿/**
    1. Fix the failing test "MessageWriterTest".
    2. You may modify the "WriteToFileAsync" method, but do not modify the method signature.
*/

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Threading.Tasks;

namespace TechnicalTasks
{
    /// <summary>
    /// Summary description for Task_2
    /// </summary>
    [TestClass]
    public class Task_2
    {
        private static readonly string _filename = AppDomain.CurrentDomain.BaseDirectory + "\\Task_2.txt";
        
        [TestInitialize]
        public void TestInit()
        {
            if(!File.Exists(_filename))
            {
                File.Create(_filename).Close();
            }
        }

        [TestCleanup]
        public void TestCleanup()
        {
            if(File.Exists(_filename))
            {
                File.Delete(_filename);
            }
        }

        [TestMethod]
        public void MessageWriterTest()
        {
            var messageWriter = new MessageWriter();
            messageWriter.WriteToFileAsync(_filename);

            using (FileStream stream = File.Open(_filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                StreamReader reader = new StreamReader(stream);
                string line = reader.ReadLine();
                Assert.AreEqual("The quick brown fox jumped over the lazy dog.", line);
            }
        }
    }
}
