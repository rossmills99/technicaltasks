﻿using System.IO;
using System.Threading.Tasks;

namespace TechnicalTasks
{
    public class MessageWriter : IMessageWriter
    {
        /// <summary>
        /// You may modify the method but you should not modify the method signature.
        /// </summary>
        /// <returns></returns>
        public async Task WriteToFileAsync(string filename)
        {
            using (FileStream stream = File.Open(filename, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                StreamWriter writer = new StreamWriter(stream);
                await writer.WriteLineAsync("The quick brown fox jumped over the lazy dog.");
                writer.WriteLine("End.");
            }
        }
    }
}
