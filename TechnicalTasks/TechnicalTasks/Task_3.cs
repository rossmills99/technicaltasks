﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using System.Collections.Generic;

namespace TechnicalTasks
{
    [TestClass]
    public class Task_3
    {
        
    }

    internal class Repository : IRepository
    {
        public IEnumerable<DataStructure> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DataStructure> GetAllDrivers()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetNames()
        {
            throw new NotImplementedException();
        }

        public DataStructure GetOldest()
        {
            throw new NotImplementedException();
        }
    }

    internal interface IRepository
    {
        IEnumerable<DataStructure> GetAll();

        IEnumerable<DataStructure> GetAllDrivers();

        IEnumerable<string> GetNames();

        DataStructure GetOldest();
    }

    internal class DataStructure
    {
        internal string Name { get; set; }

        internal int Age { get; set; }

        internal bool CanDrive
        {
            get
            {
                return Age >= 17;
            }
        }

        internal bool CanDrink
        {
            get
            {
                return Age >= 18;
            }
        }
    }
}
