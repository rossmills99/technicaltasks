﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnicalTasks
{

    /// <summary>
    /// Client code, uses an instance of IDateHelper.
    /// </summary>
    internal class DateHelperClient
    {
        private IDateHelper _dateHelper;

        internal DateHelperClient(IDateHelper dateHelper)
        {
            _dateHelper = dateHelper;
        }

        internal string DayStartOrEnd()
        {
            var now = DateTime.Now;
            return _dateHelper.GetNearestUtcDateTimeString(now.Date, now.Date.AddDays(1));
        }
    }
}
